package com.tamaton.util.conf;

import com.tamaton.util.conf.domain.Translator;
import java.sql.SQLException;
import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigurationGatewayTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationGatewayTest.class);

    private static final String ENV_NAME = "tamaton.env";

    @BeforeClass
    public static void setSystemProps(){
        System.setProperty(ENV_NAME, "DEV");
    }

    @AfterClass
    public static void removeSystemProps(){
        System.clearProperty(ENV_NAME);
    }
    
    public Configuration configuration;
    
    @Before
    public void setUp() throws Exception {
        
        configuration = ConfigurationBuilder
                .initialize()
                .setApplication("test")
                .setSubscription("subs")
                .addDefaultCache()
                .put("prop-1", "prop-1-value", false)
                .put("prop-2", "prop-2-value", false)
                .and()
                .useOfflineMode()
                .build();
    }

    @Test
    public void shouldNotThrowNPEDuringUnitTestWithNoEnvSet() throws SQLException{
        try {
            String prop = configuration.getProperty("test-prop");
            assertEquals(null, prop);
            
            String prop2 = configuration.getProperty("prop-1-value");
            assertEquals(null, prop2);
        } catch(Exception e){
            LOGGER.error("error",e);
            fail("should not have caused an Exception");
        }
    }

    @Test
    public void defaultValueTest() throws SQLException{
        try {
            String prop = configuration.getProperty("test-prop-5", "no-val");
            assertEquals("no-val", prop);
            
            int prop2 = configuration.getProperty("prop-1-value-2", Translator.translateInteger(), 55);
            assertEquals(55, prop2);
        } catch(Exception e){
            LOGGER.error("error",e);
            fail("should not have caused an Exception");
        }
    }
}
