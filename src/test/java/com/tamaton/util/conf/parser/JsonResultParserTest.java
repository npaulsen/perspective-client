/*
 *                   .-:--:-.             
 *               .:-``      ``-:.         
 *             ::`  .-/++++\-.  `::      
 *           /:  /+o          o+\  :\     
 *         ::  /+                +\  ::  
 *         |   ++++++++++++++++++++++++++++.
 *         o  ++  .```.    .```.          ++
 *         |   ++++++++++++++++++++  ++++++`
 *         ::  \++              ++/  ::  
 *          ::   \+o          o+/   ::     
 *            `:.   `-++..++-`   .:`      
 *              `-:.          .:-`        
 *                  `--:--:--`   
 */
package com.tamaton.util.conf.parser;

import com.tamaton.util.conf.domain.SettingEntry;
import java.io.InputStream;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author The.Laughing.Man
 */
public class JsonResultParserTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonResultParser.class);
    
    public JsonResultParserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of streamToValues method, of class JsonResultParser.
     */
    @Test
    public void testStreamToValues() {
        try{
            InputStream is = IOUtils.toInputStream("{\"perspective-professional-price\":{\"value\":\"149\",\"secret\":false},\"perspective-starter-price\":{\"value\":\"0\",\"secret\":false}}", "UTF-8");//mock(InputStream.class);
            
            JsonResultParser instance = new JsonResultParser();
            Map<String, Object> result = instance.streamToValues(is);
            
            SettingEntry entry1 = (SettingEntry)result.get("perspective-professional-price");
            SettingEntry entry2 = (SettingEntry)result.get("perspective-starter-price");
            
            assertEquals(2, result.values().size());
            assertEquals("149", entry1.getValue());
            assertEquals(false, entry1.isSecret());
            assertEquals("0", entry2.getValue());
            assertEquals(false, entry2.isSecret());
        }catch(Exception e){
            LOGGER.error("testStreamToValues", e);
        }
    }

    /**
     * Test of streamToValues method, of class JsonResultParser.
     */
    @Test
    public void testStreamToValuesWithSecrets() {
        try{
            InputStream is = IOUtils.toInputStream("{\"perspective-professional-price\":{\"value\":\"149\",\"secret\":false},\"perspective-starter-price\":{\"value\":\"0\",\"secret\":true},\"random-prop\":{\"value\":\"#$^%&*(!#%{}><\",\"secret\":false},\"perspective-boolean\":{\"value\":\"true\",\"secret\":false},\"what?\":{\"value\":\"what-what\",\"secret\":true}}", "UTF-8");
            
            JsonResultParser instance = new JsonResultParser();
            Map<String, Object> result = instance.streamToValues(is);
            
            SettingEntry entry1 = (SettingEntry)result.get("perspective-professional-price");
            SettingEntry entry2 = (SettingEntry)result.get("perspective-starter-price");
            SettingEntry entry3 = (SettingEntry)result.get("random-prop");
            SettingEntry entry4 = (SettingEntry)result.get("perspective-boolean");
            SettingEntry entry5 = (SettingEntry)result.get("what?");
            
            assertEquals(5, result.values().size());
            assertEquals("149", entry1.getValue());
            assertEquals(false, entry1.isSecret());
            assertEquals("0", entry2.getValue());
            assertEquals(true, entry2.isSecret());
            assertEquals("#$^%&*(!#%{}><", entry3.getValue());
            assertEquals(false, entry3.isSecret());
            assertEquals("true", entry4.getValue());
            assertEquals(false, entry4.isSecret());
            assertEquals("what-what", entry5.getValue());
            assertEquals(true, entry5.isSecret());
        }catch(Exception e){
            LOGGER.error("testStreamToValues", e);
        }
    }
}
