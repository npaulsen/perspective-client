/*
 *                   .-:--:-.             
 *               .:-``      ``-:.         
 *             ::`  .-/++++\-.  `::      
 *           /:  /+o          o+\  :\     
 *         ::  /+                +\  ::  
 *         |   ++++++++++++++++++++++++++++.
 *         o  ++  .```.    .```.          ++
 *         |   ++++++++++++++++++++  ++++++`
 *         ::  \++              ++/  ::  
 *          ::   \+o          o+/   ::     
 *            `:.   `-++..++-`   .:`      
 *              `-:.          .:-`        
 *                  `--:--:--`   
 */
package com.tamaton.util.conf;

import static com.tamaton.util.conf.client.SimplePerspectiveClient.DEFAULT_CACHE;
import com.tamaton.util.conf.exception.MissingPropertyException;
import java.util.Collections;
import java.util.Map;

/**
 * The ConfigurationBuilder class provides an easy to use constructor for the 
 * Configuration object. You can provide any number of settings or allow them to 
 * remain at the default values. 
 * <p>
 * The minimum required values are application, subscription, email and
 * accountId. 
 * 
 * @author The.Laughing.Man
 */
public class ConfigurationBuilder {
    private boolean offlineMode = false;
    private Map<String, Object> defaultCache = Collections.EMPTY_MAP;
    private String environment = "*";
    private String application = null;
    private String subscription = null;
    private String email;
    private String accountId;
    private String instance = "";
    private long cacheTime = DEFAULT_CACHE;

    public ConfigurationBuilder() {}
    
    /**
     * Static method for initializing a new ConfigurationBuilder. Can also use
     * new ConfigurationBuilder();
     * 
     * @return ConfigurationBuilder
     */
    public static ConfigurationBuilder initialize() {
        return new ConfigurationBuilder();
    }

    public ConfigurationBuilder setEmail(String email) {
        this.email = email;
        
        return this;
    }

    /**
     * Set the accountId associated with this project. Provided at 
     * portal.tamaton.com
     * 
     * @param accountId
     * @return this instance of ConfigurationBuilder
     */
    public ConfigurationBuilder setAccountId(String accountId) {
        this.accountId = accountId;
        
        return this;
    }

    /**
     * Set the dedicated instance name associated with this project. Provided at 
     * portal.tamaton.com for those using an enterprise subscription.
     * <p>
     * The default value is blank (works for shared instances).
     * 
     * @param instance
     * @return this instance of ConfigurationBuilder
     */
    public ConfigurationBuilder setInstance(String instance) {
        this.instance = instance;
        
        return this;
    }

    /**
     * Set the environment where this is currently deployed. This value should
     * match one of the environment values you provided at portal.tamaton.com
     * <p>
     * The default value is "*" and will pull only generic settings from
     * portal.tamaton.com 
     * 
     * @param environment
     * @return this instance of ConfigurationBuilder
     */
    public ConfigurationBuilder setEnvironment(String environment) {
        this.environment = environment;
        
        return this;
    }

    /**
     * Set the application name of this project. This value should
     * match the application name you provided at portal.tamaton.com
     * 
     * @param application
     * @return this instance of ConfigurationBuilder
     */
    public ConfigurationBuilder setApplication(String application) {
        this.application = application;
        
        return this;
    }

    /**
     * Set the subscription associated with this project. Provided at 
     * portal.tamaton.com
     * 
     * @param subscription
     * @return this instance of ConfigurationBuilder
     */
    public ConfigurationBuilder setSubscription(String subscription) {
        this.subscription = subscription;
        
        return this;
    }

    /**
     * Set the cacheTime for the settings used in this project in milliseconds. 
     * <p>
     * The default value is 10 min, the minimum value is 10 seconds and the
     * maximum value is 24 hours.
     * 
     * @param cacheTime
     * @return this instance of ConfigurationBuilder
     */
    public ConfigurationBuilder setCacheTime(long cacheTime) {
        this.cacheTime = cacheTime;
        
        return this;
    }

    /**
     * Set the Configuration to work only offline. Will not connect to 
     * portal.tamaton.com but only pull values provided in DefaultCache.
     * <p>
     * The default value is fales.
     * 
     * @return this instance of ConfigurationBuilder
     */
    public ConfigurationBuilder useOfflineMode() {
        this.offlineMode = true;
        
        return this;
    }

    /**
     * Create a SettingsBuilder object to create a default cache instance. Will
     * be used on startup prior to connecting to portal.tamaton.com
     * <p>
     * The default value is an empty set.
     * 
     * @return new instance of SettingsBuilder
     */
    public SettingsBuilder addDefaultCache() {
        
        return new SettingsBuilder(this);
    }
    
    /**
     * Set the defaultCache for this project. Will be used on startup prior to 
     * connecting to portal.tamaton.com
     * 
     * @param defaultCache
     * @return this instance of ConfigurationBuilder
     */
    void setDefaultCache(Map<String, Object> defaultCache) {
        this.defaultCache = defaultCache;
    }
    
    /**
     * Builds the Configuration object based on provided values. 
     * 
     * @return the completed Configuration object to use for pulling settings.
     */
    public Configuration build(){
        if(!offlineMode){
            if(application==null){
                throw new MissingPropertyException("Mising required property: application. This should match the application name provided in the Perspective UI.");
            }
            if(subscription==null){
                throw new MissingPropertyException("Mising required property: subscription. This should match the subscription key provided in the Perspective UI.");
            }
            if(email==null){
                throw new MissingPropertyException("Mising required property: email. This should match the email used to log in to the Perspective UI.");
            }
            if(accountId==null){
                throw new MissingPropertyException("Mising required property: accountId. This should match the account id of the email provided. Can be found in the Perspective UI. Account ID is sensitive. You should keep this secret for users and create an application user to use in apps.");
            }
        }else if(defaultCache==null || defaultCache.isEmpty()){
            throw new MissingPropertyException("Mising required property defaultCache while in offline mode. Must provide a default cache to use in offline mode.");
        }
        return new ConfigurationGateway(email, accountId, application, subscription, instance, environment, cacheTime, defaultCache, offlineMode);
    }
}
