package com.tamaton.util.conf.exception;

/**
 * Created with IntelliJ IDEA.
 * User: Ken.Copeland
 * Date: 2/6/14
 * Time: 1:22 PM
 *
 */
public class NotFoundException extends LookupException {
    NotFoundException(String msg) { super(msg); }
}
