package com.tamaton.util.conf.exception;

public class LookupException extends Exception {
    LookupException(String msg) { super(msg); }
}
