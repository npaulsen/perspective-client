/*
 *                   .-:--:-.             
 *               .:-``      ``-:.         
 *             ::`  .-/++++\-.  `::      
 *           /:  /+o          o+\  :\     
 *         ::  /+                +\  ::  
 *         |   ++++++++++++++++++++++++++++.
 *         o  ++  .```.    .```.          ++
 *         |   ++++++++++++++++++++  ++++++`
 *         ::  \++              ++/  ::  
 *          ::   \+o          o+/   ::     
 *            `:.   `-++..++-`   .:`      
 *              `-:.          .:-`        
 *                  `--:--:--`   
 */
package com.tamaton.util.conf.exception;

/**
 *
 * @author The.Laughing.Man
 */
public class MalformedUrlException extends RuntimeException{
    public MalformedUrlException(String e){super(e);}
}
