package com.tamaton.util.conf.exception;

public class MissingPropertyException extends RuntimeException {
    public MissingPropertyException(String msg) { super(msg); }
}
