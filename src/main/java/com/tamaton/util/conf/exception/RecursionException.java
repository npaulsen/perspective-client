package com.tamaton.util.conf.exception;

/**
 * Created with IntelliJ IDEA.
 * User: Ken.Copeland
 * Date: 2/6/14
 * Time: 11:37 AM
 *
 */


public class RecursionException extends LookupException {
    RecursionException(String msg) { super(msg); }
}