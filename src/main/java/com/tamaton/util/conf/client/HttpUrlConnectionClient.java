/*
 *                   .-:--:-.             
 *               .:-``      ``-:.         
 *             ::`  .-/++++\-.  `::      
 *           /:  /+o          o+\  :\     
 *         ::  /+                +\  ::  
 *         |   ++++++++++++++++++++++++++++.
 *         o  ++  .```.    .```.          ++
 *         |   ++++++++++++++++++++  ++++++`
 *         ::  \++              ++/  ::  
 *          ::   \+o          o+/   ::     
 *            `:.   `-++..++-`   .:`      
 *              `-:.          .:-`        
 *                  `--:--:--`   
 */
package com.tamaton.util.conf.client;

import com.tamaton.util.conf.parser.JsonResultParser;
import com.tamaton.util.conf.parser.ResultParser;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author The.Laughing.Man
 */
public class HttpUrlConnectionClient implements HttpClient {
    private static final String CONTENT_TYPE = "content-type";
    private static final String APPLICATION_JSON = "application/json";
    private static final String BASIC_AUTH = "Basic YmFzaWMtdXNlcjohbjB0aGluZy10MC1zMzMtaGVyZSE=";
    private static final String POST = "POST";
    private static final int CONNECT_TIMEOUT = 5000;
    private static final int REQUEST_TIMEOUT = 30000;
    
    private static Logger LOGGER = Logger.getLogger(HttpUrlConnectionClient.class.getName());
    
    private final ResultParser resultParser;
    
    HttpUrlConnectionClient() throws IOException{
        resultParser = new JsonResultParser();
    }
    
    @Override
    public Map<String, Object> post(String uri, String jsonBody) throws IOException{
        return executeRequest(uri, jsonBody);
    }
    
    private Map<String, Object> executeRequest(String targetURL, String urlParameters) {
        HttpURLConnection connection = null;
        
        Map<String, Object> result = Collections.EMPTY_MAP;
        
        try {
            //Create connection
            URL url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(POST);
            connection.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON);
            connection.setRequestProperty("Authorization", BASIC_AUTH);
            
            connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");  

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream (connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.close();

            //Get Response  
            InputStream is = connection.getInputStream();
            
            result = resultParser.streamToValues(is);
            
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        
        LOGGER.debug("executed Request");
        
        return result;
    }
}
