/*
 *                   .-:--:-.             
 *               .:-``      ``-:.         
 *             ::`  .-/++++\-.  `::      
 *           /:  /+o          o+\  :\     
 *         ::  /+                +\  ::  
 *         |   ++++++++++++++++++++++++++++.
 *         o  ++  .```.    .```.          ++
 *         |   ++++++++++++++++++++  ++++++`
 *         ::  \++              ++/  ::  
 *          ::   \+o          o+/   ::     
 *            `:.   `-++..++-`   .:`      
 *              `-:.          .:-`        
 *                  `--:--:--`   
 */
package com.tamaton.util.conf.client;

import java.io.IOException;

/**
 *
 * @author The.Laughing.Man
 */
public interface PerspectiveClient {
    void shutdown();
    void getAllProperties() throws IOException;
}
