/*
 *                   .-:--:-.             
 *               .:-``      ``-:.         
 *             ::`  .-/++++\-.  `::      
 *           /:  /+o          o+\  :\     
 *         ::  /+                +\  ::  
 *         |   ++++++++++++++++++++++++++++.
 *         o  ++  .```.    .```.          ++
 *         |   ++++++++++++++++++++  ++++++`
 *         ::  \++              ++/  ::  
 *          ::   \+o          o+/   ::     
 *            `:.   `-++..++-`   .:`      
 *              `-:.          .:-`        
 *                  `--:--:--`   
 */
package com.tamaton.util.conf.client;

import com.tamaton.util.conf.ConfigurationGateway;
import com.tamaton.util.conf.domain.DaemonThreadFactory;
import com.tamaton.util.conf.domain.Translator;
import com.tamaton.util.conf.exception.MalformedUrlException;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

/**
 *
 * @author The.Laughing.Man
 */
public class SimplePerspectiveClient implements PerspectiveClient {
    
    private static Logger LOGGER = Logger.getLogger(SimplePerspectiveClient.class.getName());
    
    public static final long DEFAULT_CACHE = 10 * 60 * 1000;
    public static final long MIN_CACHE_MILLIS = 10 * 1000;
    public static final long MAX_CACHE_MILLIS = 24 * 60 * 60 * 1000;
    
    public static final String WILD_CARD = "*";
    public static final String CACHE_OVERRIDE_SETTING = "perspective-cache-time";
    
    private static final Translator LONG_TRANSLATOR = Translator.translateLong();
    
    private final String URL;
    private final String JSON;
    
    private ScheduledFuture future;
    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(0, new DaemonThreadFactory());
    private final HttpClient httpClient;
    private final ConfigurationGateway configuration;
    private final long cacheTime;
    
    public SimplePerspectiveClient(ConfigurationGateway configuration, String email, String accountId, String application, String subscription, String instance, String environment, long cacheTime) throws IOException{
        String urlInstance;
        if(instance == null || "".equals(instance)){
            urlInstance = "";
        }else if(instance.contains(" ")){
            throw new MalformedUrlException("Instance cannot contain white space. If you do not have a dedicated instance, leave blank or null.");
        }else{
            urlInstance = instance+".";
        }
        URL = "https://"+urlInstance+"services.tamaton.com/perspective/api/get-parameters.do";
        JSON = generateJsonRequest(accountId, email, subscription, application, environment);
        
        this.cacheTime = cacheTime;
        this.configuration = configuration;
        
        httpClient = new HttpUrlConnectionClient();
        //fails on startup but then works smoothly. also needs correct timer:
        future = scheduledExecutorService.schedule(new PerspectiveRunnable(), 1000, TimeUnit.MILLISECONDS);
        
        Runtime.getRuntime().addShutdownHook(new Thread(new ShutdownRunnable()));
    }
    
    @Override
    public void getAllProperties() throws IOException{
        httpClient.post(URL, JSON);
    }
    
    private String generateJsonRequest(String accountId, String email, String subscription, String application, String environment){
        return "{" +
                    "\"accountKey\": {" +
                        "\"accountId\":\"" + accountId + "\"," +
                        "\"email\":\"" + email + "\"" +
                    "}," +
                   "\"apiKey\":\"" + subscription + "\"," +
                   "\"application\":\"" + application + "\"," +
                   "\"environment\":\"" + environment + "\"" +
                "}";
    }
    
    private class PerspectiveRunnable implements Runnable{
        
        @Override
        public void run() {
            Map<String, Object> response = Collections.EMPTY_MAP;
            try{
                response = httpClient.post(URL, JSON);
            }catch(Exception e){
                LOGGER.error("PerspectiveRunnable", e);
            }
            
            if(!response.isEmpty()){
                configuration.setCache(response);
            }
            
            if(!scheduledExecutorService.isShutdown()){
                future = scheduledExecutorService.schedule(new PerspectiveRunnable(), getCachingMillis(), TimeUnit.MILLISECONDS);
            }
        }
    }
    
    
    private class ShutdownRunnable implements Runnable{
        
        @Override
        public void run() {
            System.out.println("ShutdownRunnable called");
            shutdown();
        }
    }
    
    @Override
    public void shutdown() {
        System.out.println("shutdown called");
        try {
            future.cancel(true);
        } catch (Exception e) { }
        try {
            scheduledExecutorService.shutdown();
            
            if (!scheduledExecutorService.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                scheduledExecutorService.shutdownNow();
            } 
        } catch (InterruptedException e) {
            scheduledExecutorService.shutdownNow();
        }
    }
    
    /**
     * Sets the cache in milliseconds. This will be how long properties will
     * live in cache before the configuration goes to the database again. This is an optional field and defaults to
     * ten seconds (10000ms). Input values are limited to the range of MIN_CACHE_MILLIS and MAX_CACHE_MILLIS.
     *
     * @see MIN_CACHE_MILLIS
     * @see MAX_CACHE_MILLIS
     * 
     * @return cache time in ms
     */
    public final long getCachingMillis() {
        long millis = configuration.getProperty(CACHE_OVERRIDE_SETTING, LONG_TRANSLATOR, cacheTime);
        
        if (millis < MIN_CACHE_MILLIS){
            LOGGER.warn("perspective : cache millis was below the minimum value, setting to (" + MIN_CACHE_MILLIS + ") millis.");
            millis = MIN_CACHE_MILLIS;
        }

        if (millis > MAX_CACHE_MILLIS) {
            LOGGER.warn("perspective : cache millis was above the maximum value, setting to (" + MAX_CACHE_MILLIS + ") millis.");
            millis = MAX_CACHE_MILLIS;
        }

        return millis;
    }
}
