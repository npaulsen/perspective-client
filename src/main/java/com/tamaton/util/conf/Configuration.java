package com.tamaton.util.conf;

import com.tamaton.util.conf.domain.Translator;

/**
 * Interface for getting a property based on the environment, application and property name.
 *
 * @author the.laughing.man
 */
public interface Configuration {

    /**
     * Tries to retrieve a property from the database based on the environment and application.  Search steps as follow:
     * <p>
     * name - environment - application<br>
     * name -     *       - application<br>
     * name - environment -      *     <br>
     * name -     *       -      *     <br>
     * <p>
     * This lets us get the property named &quot;name&quot; for the current environment and this application if it exists.  Next
     * check for the catch-all environment for this application.  Next check for this environment, again, but for the
     * catch- all application.  Next we check for the property named &quot;name&quot; for the catch-all environment and application.
     *
     * @param name The name of the property to search for
     *
     * @return The value of the property or null if none
     */
    String getProperty(String name);
    
    /**
     * Tries to retrieve a property from the database based on the environment and application.  Search steps as follow:
     * <p>
     * name - environment - application<br>
     * name -     *       - application<br>
     * name - environment -      *     <br>
     * name -     *       -      *     <br>
     * <p>
     * This lets us get the property named &quot;name&quot; for the current environment and this application if it exists.  Next
     * check for the catch-all environment for this application.  Next check for this environment, again, but for the
     * catch- all application.  Next we check for the property named &quot;name&quot; for the catch-all environment and application.
     *
     * @param name The name of the property to search for
     * @param defaultValue The value to return if no setting is found
     *
     * @return The value of the property or defaultValue if no value is found
     */
    String getProperty(String name, String defaultValue);
    
    /**
     * Tries to retrieve a property from the database based on the environment and application.  Search steps as follow:
     * <p>
     * name - environment - application<br>
     * name -     *       - application<br>
     * name - environment -      *     <br>
     * name -     *       -      *     <br>
     * <p>
     * This lets us get the property named &quot;name&quot; for the current environment and this application if it exists.  Next
     * check for the catch-all environment for this application.  Next check for this environment, again, but for the
     * catch- all application.  Next we check for the property named &quot;name&quot; for the catch-all environment and application.
     *
     * @param name The name of the property to search for
     * @param translater An extension of type Translator to convert from a property String to provided object type. Throws
     * null pointer exception if null (use String getProperty(String name) if you want a string).
     *
     * @return The value of the property or null if none
     */
    <E> E getProperty(String name, Translator<E> translater);
    
    
    /**
     * Tries to retrieve a property from the database based on the environment and application.  Search steps as follow:
     * <p>
     * name - environment - application<br>
     * name -     *       - application<br>
     * name - environment -      *     <br>
     * name -     *       -      *     <br>
     * <p>
     * This lets us get the property named &quot;name&quot; for the current environment and this application if it exists.  Next
     * check for the catch-all environment for this application.  Next check for this environment, again, but for the
     * catch- all application.  Next we check for the property named &quot;name&quot; for the catch-all environment and application.
     *
     * @param name The name of the property to search for
     * @param translater An extension of type Translator to convert from a property String to provided object type. Throws
     * null pointer exception if null (use String getProperty(String name) if you want a string).
     * @param defaultValue The value to return if no setting is found
     *
     * @return The value of the property or defaultValue if no value is found
     */
    <E> E getProperty(String name, Translator<E> translater, E defaultValue);
    
    /**
     * Shuts down Executor. Must be called for clean application close.
     */
    void shutdown();
}
