/*
 *                   .-:--:-.             
 *               .:-``      ``-:.         
 *             ::`  .-/++++\-.  `::      
 *           /:  /+o          o+\  :\     
 *         ::  /+                +\  ::  
 *         |   ++++++++++++++++++++++++++++.
 *         o  ++  .```.    .```.          ++
 *         |   ++++++++++++++++++++  ++++++`
 *         ::  \++              ++/  ::  
 *          ::   \+o          o+/   ::     
 *            `:.   `-++..++-`   .:`      
 *              `-:.          .:-`        
 *                  `--:--:--`   
 */
package com.tamaton.util.conf.parser;

import com.tamaton.util.conf.ConfigurationGateway;
import com.tamaton.util.conf.domain.SettingEntry;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author The.Laughing.Man
 */
public class JsonResultParser implements ResultParser {
    private static Logger LOGGER = Logger.getLogger(JsonResultParser.class.getName());
    
    private static final String UTF_8 = "UTF-8";
    
    @Override
    public Map<String, Object> streamToValues(InputStream inputStream){
        Map<String, Object> objects = new HashMap();
        StringBuilder response = new StringBuilder();
        try{
            Reader reader = new InputStreamReader(inputStream, UTF_8);
            
            String name = null;
            SettingEntry entry = new SettingEntry();
            StringBuilder builder = new StringBuilder();
            boolean record = false;
            boolean recordName = true;
            boolean recordSecret = false;
            int r;
            while ((r = reader.read()) != -1) {
                char ch = (char) r;
                
                response.append(ch);
                
                if(ch == '"'){
                    if(record){
                        String output = builder.substring(1);
                        builder = new StringBuilder();
                        
                        if("secret".equals(output)){
                            recordSecret = true;
                        }else if(recordName){
                            name = output;
                            recordName = false;
                        }else{
                            entry.setValue(output);
                        }
                        
                    }
                    record = !record;
                }
                
                if(recordSecret){
                    if(ch == 't' || ch == 'T'){
                        recordSecret = false;
                        entry.setSecret(true);
                        objects.put(name, entry);
                    } else if(ch == 'f' || ch == 'F'){
                        recordSecret = false;
                        entry.setSecret(false);
                    }
                    if(!recordSecret){
                        objects.put(name, entry);
                        entry = new SettingEntry();
                        recordName = true;
                    }
                }
                
                if(record){
                    builder.append(ch);
                }
            }
        }catch(Exception e){
            LOGGER.error("streamToValues", e);
        }
        
        LOGGER.debug("response: "+ response.toString());
        
        return objects;
    }
}
