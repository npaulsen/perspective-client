/*
 * @Copyright 2014: Not for reuse or resale, All rights reserved by Paulsen LLC
 */
package com.tamaton.util.conf.domain;

import java.util.concurrent.ThreadFactory;

/**
 *
 * @author Norm
 */
public class DaemonThreadFactory implements ThreadFactory {
    
    @Override
    public Thread newThread(Runnable r) {
        Thread thread = new Thread(r);
        
        thread.setDaemon(true);
        
        return thread;
    }
}