/*
 *                   .-:--:-.             
 *               .:-``      ``-:.         
 *             ::`  .-/++++\-.  `::      
 *           /:  /+o          o+\  :\     
 *         ::  /+                +\  ::  
 *         |   ++++++++++++++++++++++++++++.
 *         o  ++  .```.    .```.          ++
 *         |   ++++++++++++++++++++  ++++++`
 *         ::  \++              ++/  ::  
 *          ::   \+o          o+/   ::     
 *            `:.   `-++..++-`   .:`      
 *              `-:.          .:-`        
 *                  `--:--:--`   
 */
package com.tamaton.util.conf.domain;

/**
 *
 * @author The.Laughing.Man
 */
public class SettingEntry {
    public static final String MASKED_VALUE = "XXXXX";
    private String value;
    private boolean secret = true;

    public SettingEntry(String value, boolean secret) {
        this.value = value;
        this.secret = secret;
    }
    
    public SettingEntry() {}

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isSecret() {
        return secret;
    }

    public void setSecret(boolean secret) {
        this.secret = secret;
    }

    @Override
    public String toString() {
        if(secret){
            return MASKED_VALUE;
        }else{
            return value;
        }
    }
}
