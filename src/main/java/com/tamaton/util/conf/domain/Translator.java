/*
 *                   .-:--:-.             
 *               .:-``      ``-:.         
 *             ::`  .-/++++\-.  `::      
 *           /:  /+o          o+\  :\     
 *         ::  /+                +\  ::  
 *         |   ++++++++++++++++++++++++++++.
 *         o  ++  .```.    .```.          ++
 *         |   ++++++++++++++++++++  ++++++`
 *         ::  \++              ++/  ::  
 *          ::   \+o          o+/   ::     
 *            `:.   `-++..++-`   .:`      
 *              `-:.          .:-`        
 *                  `--:--:--`   
 */
package com.tamaton.util.conf.domain;

import java.math.BigDecimal;
import java.util.regex.Pattern;

/**
 * 
 * @author The.Laughing.Man
 * @param <T> 
 */
public abstract class Translator<T> {
    public abstract T translate(String s);
    
    public static Translator<Integer> translateInteger(){
        return new IntegerTranslater();
    }
    
    /**
     * Returns a new instance of LongTranslater. 
     * 
     * If incoming value is null returns null, else returns Long.valueOf
     * 
     * @return 
     */
    public static Translator<Long> translateLong(){
        return new LongTranslater();
    }
    
    /**
     * Returns a new instance of DoubleTranslater. 
     * 
     * If incoming value is null returns null, else returns Double.valueOf
     * 
     * @return 
     */
    public static Translator<Double> translateDouble(){
        return new DoubleTranslater();
    }
    
    /**
     * Returns a new instance of BigDecimalTranslater. 
     * 
     * If incoming value is null returns null, else returns new BigDecimal(string)
     * 
     * @return 
     */
    public static Translator<BigDecimal> translateBigDecimal(){
        return new BigDecimalTranslater();
    }
    
    /**
     * Returns a new instance of ArrayTranslater. 
     * 
     * If incoming value is null returns null, else returns string.split(",")
     * 
     * @return 
     */
    public static Translator<String[]> translateCommaDelimitedArray(){
        return new ArrayTranslater();
    }
    
    /**
     * Returns a new instance of PatternTranslater. 
     * 
     * Returns a Pattern created by Pattern.compile(value);
     * 
     * @return 
     */
    public static Translator<Pattern> translatePattern(){
        return new PatternTranslater();
    }

    private static class IntegerTranslater extends Translator<Integer> {
        @Override
        public Integer translate(String s){
            if(s==null){return null;}
            return Integer.valueOf(s);
        }
    }
    
    private static class LongTranslater extends Translator<Long> {
        @Override
        public Long translate(String s){
            if(s==null){return null;}
            return Long.valueOf(s);
        }
    }
    
    private static class DoubleTranslater extends Translator<Double> {
        @Override
        public Double translate(String s){
            if(s==null){return null;}
            return Double.valueOf(s);
        }
    }
    
    private static class BigDecimalTranslater extends Translator<BigDecimal> {
        @Override
        public BigDecimal translate(String s){
            if(s==null){return null;}
            return new BigDecimal(s);
        }
    }
    
    private static class ArrayTranslater extends Translator<String[]> {
        private final String delimiter = ",";
        
        @Override
        public String[] translate(String s){
            if(s==null){return null;}
            return s.split(delimiter);
        }
    }
    
    private static class PatternTranslater extends Translator<Pattern> {
        @Override
        public Pattern translate(String s){
            return Pattern.compile(s);
        }
    }
}
