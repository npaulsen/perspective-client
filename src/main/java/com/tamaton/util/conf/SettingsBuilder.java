/*
 *                    .-:--:-.             
 *                .:-``      ``-:.         
 *              ::`  .-/++++\-.  `::      
 *            /:  /+o          o+\  :\     
 *          ::  /+                +\  ::  
 *          |   ++++++++++++++++++++++++++++.
 *          o  ++  .```.    .```.          ++
 *          |   ++++++++++++++++++++  ++++++`
 *          ::  \++              ++/  ::  
 *           ::   \+o          o+/   ::     
 *             `:.   `-++..++-`   .:`      
 *               `-:.          .:-`        
 *                   `--:--:--`   
 *  
 * @Copyright 2014: Not for reuse or resale, All rights reserved by Tamaton LLC
 */
package com.tamaton.util.conf;

import com.tamaton.util.conf.domain.SettingEntry;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author The.Laughing.Man
 */
public class SettingsBuilder {
    private final ConfigurationBuilder configurationBuilder;
    private final Map<String, Object> defaultCache = new HashMap();
    
    SettingsBuilder(ConfigurationBuilder configurationBuilder){
        this.configurationBuilder = configurationBuilder;
        configurationBuilder.setDefaultCache(defaultCache);
    }
    
    public SettingsBuilder put(String name, String value){
        defaultCache.put(name, new SettingEntry(value, true));
        
        return this;
    }
    
    public SettingsBuilder put(String name, String value, boolean secret){
        defaultCache.put(name, new SettingEntry(value, secret));
        
        return this;
    }
    
    public ConfigurationBuilder and(){
        return configurationBuilder;
    }
}
