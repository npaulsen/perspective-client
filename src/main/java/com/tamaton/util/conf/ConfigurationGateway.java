package com.tamaton.util.conf;

import com.tamaton.util.conf.client.PerspectiveClient;
import com.tamaton.util.conf.client.SimplePerspectiveClient;
import static com.tamaton.util.conf.client.SimplePerspectiveClient.DEFAULT_CACHE;
import com.tamaton.util.conf.domain.SettingEntry;
import com.tamaton.util.conf.domain.Translator;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.log4j.Logger;

/**
 * This implementation of Configuration contains an ObjectCache to load string representations of configuration
 * Objects and reconstitute them into Objects via Translator(s), and then cache these Objects for subsequent requests.
 *
 * Multiple Translators can be utilized to reconstitute them into Objects, and each is cached separately by modifying the property name.
 * Ultimately the original String representation is retained and that is what is used for any new reconstitution.
 *
 * Finally the cache is refreshed based on the cacheTime parameter and only one thread is allowed to initiate refresh due to the AtomicBoolean.
 *
 * DEFAULT Standard Layering can be achieved using the constructor that has the ObjectCacheConfiguration(datasource, ConfigSearchParams, cacheTime, ApplicationName).
 * The Default Layers looks like the following:
 *
 * | Interface Name    | Implementation Class Name | Comments
 * |-------------------|---------------------------|-------------------------------------------------------------------------------------
 * | Configuration     | ConfigurationGateway      | This layer provides access to properties cache and translator functions
 * | PerspectiveClient | SimplePerspectiveClient   | This layer stores cache and asynchronously updates cache
 * | HttpClient        | HttpUrlConnectionClient   | This layer performs calls to the backend Perspective server for property updates
 * 
 * @author the.laughing.man
 * @since 2/3/2020
 */
public class ConfigurationGateway implements Configuration {
    
    /**
     * can pull env using String env = System.getProperty(SYS_VAR_ENV_NAME); and pass into object
     */
    
    
    public static final String SEPARATOR = "-";
    
    private static Logger LOGGER = Logger.getLogger(ConfigurationGateway.class.getName());
    
    private PerspectiveClient perspectiveClient;
    
    private boolean offlineMode = false;
    private Map<String, Object> objectCache = Collections.EMPTY_MAP;
    private long lastUpdateTime = 0;
    private final AtomicBoolean updating = new AtomicBoolean(true);

    public ConfigurationGateway(String email, String accountId, String application, String subscription, String instance, String environment, long cacheTime, Map<String, Object> defaultCache, boolean offlineMode) {
        this(email, accountId, application, subscription, instance, environment, cacheTime, defaultCache);
        this.offlineMode = offlineMode;
    }

    public ConfigurationGateway(String email, String accountId, String application, String subscription, String instance, String environment, long cacheTime, Map<String, Object> defaultCache) {
        this(email, accountId, application, subscription, instance, environment, cacheTime);
        this.objectCache = defaultCache;
    }

    public ConfigurationGateway(String email, String accountId, String application, String subscription, String instance, String environment) {
        this(email, accountId, application, subscription, instance, environment, DEFAULT_CACHE);
    }
    
    public ConfigurationGateway(String email, String accountId, String application, String subscription, String environment, long cacheTime, Map<String, Object> defaultCache, boolean offlineMode) {
        this(email, accountId, application, subscription, environment, cacheTime, defaultCache);
        this.offlineMode = offlineMode;
    }

    public ConfigurationGateway(String email, String accountId, String application, String subscription, String environment, long cacheTime, Map<String, Object> defaultCache) {
        this(email, accountId, application, subscription, environment, cacheTime);
        this.objectCache = defaultCache;
    }

    public ConfigurationGateway(String email, String accountId, String application, String subscription, String environment, long cacheTime) {
        this(email, accountId, application, subscription, "", environment);
    }
    
    public ConfigurationGateway(String email, String accountId, String application, String subscription, String instance, String environment, long cacheTime) {
        if(instance==null){
            instance = "";
        }
        try{
            perspectiveClient = new SimplePerspectiveClient(this, email, accountId, application, subscription, instance, environment, cacheTime);
        }catch(Exception e){
            LOGGER.error("ConfigurationGateway failed to start", e);
        }
    }
    
    public ConfigurationGateway(String email, String accountId, String application, String subscription, String instance) {
        this(email, accountId, application, subscription, instance, "*");
    }
    
    public ConfigurationGateway(String email, String accountId, String application, String subscription) {
        this(email, accountId, application, subscription, "", "*");
    }
    
    protected Map<String, Object> getCache(){
        return Collections.unmodifiableMap(objectCache);
    }
    
    public void setCache(Map<String, Object> objectCache){
        
        synchronized(objectCache){
            this.objectCache = objectCache;
        }
    }
    
    /**
     * Normal call for String Values
     * @param name The name (without Translator class suffix) of the property to search for
     *
     * @return
     */
    @Override
    public final String getProperty(String name) {
        if (name == null) {
            throw new NullPointerException("Configuration Propertie's 'name' can't be null!");
        }
        
        Object object = objectCache.get(name);
        
        if(object==null){
            return null;
        }else{
            return ((SettingEntry)object).getValue();
        }
    }
    
    @Override
    public final String getProperty(String name, String defaultValue) {
        String value = getProperty(name);
        
        if(value==null){
            LOGGER.debug("getProperty defaultValue returned for "+name);
            return defaultValue;
        }else{
            return value;
        }
    }

    @Override
    public <E> E getProperty(String name, Translator<E> translator) {
        if (name == null) {
            throw new NullPointerException("Configuration Propertie's 'name' can't be null!");
        }
        if (translator == null) {
            throw new NullPointerException("Configuration Propertie's 'translator' can't be null for name: " + name);
        }

        E prop = (E) translateIfNeeded(name, translator);
        return prop;
    }

    @Override
    public <E> E getProperty(String name, Translator<E> translator, E defaultValue) {
        Object value = getProperty(name, translator);
        
        if(value==null){
            LOGGER.debug("getProperty defaultValue returned for "+name);
            return defaultValue;
        }else{
            return (E)value;
        }
    }

    @Override
    public void shutdown() {
        perspectiveClient.shutdown();
    }

    private <E> E translateIfNeeded(String name, Translator<E> translator) {
        String key = name + SEPARATOR + translator.getClass().getSimpleName();
        E prop = (E) objectCache.get(key);
        if (prop == null) {
            // get the original String value, and translate the Value Object, and Cache it
            prop = translator.translate(getProperty(name));
            objectCache.put(key, prop);
        }
        LOGGER.debug("getProperty key="+key+", value="+prop);
        return prop;
    }
}
